#include "pch.h"
#include <iostream>
#include <vector>
#include <time.h>
#include <string>

using namespace std;

void empHandDraw(vector<string>& cList) // Hand for emperor
{

	cList.push_back("Emperor");
	for (int x = 0; x < 4; x++)
	{
		cList.push_back("Civilian");
	}

}

void slaveHandDraw(vector<string>& cList) // Hand for slave
{

	cList.push_back("Slave");
	for (int x = 0; x < 4; x++)
	{
		cList.push_back("Civilian");
	}

}

void printList(vector<string> cList, int size) // Prints card list
{

	for (int x = 0; x < size; x++)
	{
		cout << "[" << x + 1 << "] " << cList[x] << endl;
	}

}

void giveCards(vector<string>& player, vector<string>& ai, int round) // Distributes cards
{

	if (round <= 3 || (round >= 7 && round <= 9))
	{
		empHandDraw(player);
		slaveHandDraw(ai);
	}
	else
	{
		slaveHandDraw(player);
		empHandDraw(ai);
	}

}

void printPlayer(int round) // Prints player's side
{

	if (round <= 3 || (round >= 7 && round <= 9))
	{
		cout << "You are: Emperor!" << endl;
	}
	else
	{
		cout << "You are: Slave!" << endl;
	}

}

void getBet(int& availDistance, int& distanceBet) // Gets millimeters wagered
{

	while (true)
	{
		cout << "And just how many millimeters would you like to wager, punk? ";
		cin >> distanceBet;
		if (distanceBet <= availDistance)
		{
			break;
		}
	}

}

void evalAndPay(vector<string>& Player, vector<string>& Ai, int playerC, int aiC, int& playerGold, int multiplier, int& distLeft, int distBet, bool& draw) // Evaluates match, pays back
{

	string playerCard = Player[playerC];
	string aiCard = Ai[aiC];
	int gatheredGold = 0;

	cout << "Ready, show!" << endl;
	cout << "Punk: " << playerCard << " vs Champ: " << aiCard << endl << endl;
	if (playerCard == "Slave" && aiCard == "Emperor") // Win condition
	{
		gatheredGold = 100000 * multiplier;
		playerGold += gatheredGold;
		cout << "You've just won " << gatheredGold << " yen!" << endl;
		draw = false;
	}
	else if (playerCard == "Emperor" && aiCard == "Slave") // Lose condition
	{
		distLeft -= distBet;

		cout << "You lost!" << endl;
		cout << "Tsk! The drill's " << distBet << " millimeters closer to your eardrum!" << endl;
		draw = false;
	}
	else if (playerCard == "Emperor" && aiCard == "Civilian") // Win condition
	{
		gatheredGold = 100000 * multiplier;
		playerGold += gatheredGold;
		cout << "You've just won " << gatheredGold << " yen!" << endl;
		draw = false;
	}
	else if (playerCard == "Slave" && aiCard == "Civilian") // Lose condition
	{
		distLeft -= distBet;

		cout << "You lost!" << endl;
		cout << "Tsk! The drill's " << distBet << " millimeters closer to your eardrum!" << endl;
		draw = false;
	}
	else if (playerCard == "Civilian" && aiCard == "Slave") // Win condition
	{
		gatheredGold = 100000 * multiplier;
		playerGold += gatheredGold;
		cout << "You've just won " << gatheredGold << " yen!" << endl;
		draw = false;
	}
	else if (playerCard == "Civilian" && aiCard == "Emperor") // Lose condition
	{
		distLeft -= distBet;

		cout << "You lost!" << endl;
		cout << "Tsk! The drill's " << distBet << " millimeters closer to your eardrum!" << endl;
		draw = false;
	}
	else // Draw
	{
		cout << "Phew! That's a breather, it's a draw!" << endl;
		draw = true;
		Player.erase(Player.begin() + playerC);
		Ai.erase(Ai.begin() + aiC);
	}
	
	system("pause");
	system("cls");
}

int winMultiplier(int round, int bet)
{
	int multiplier;
	if (round <= 3 || (round >= 7 && round <= 9))
	{
		return multiplier = 1 * bet;
	}
	else
	{
		return multiplier = 5 * bet;
	}
}

int playerChoice(int handSize)
{
	int cardChoice;
	while (true)
	{
		cin >> cardChoice;
		if (cardChoice <= handSize)
		{
			break;
		}
		cout << "You've entered an invalid card." << endl;
		system("pause");
	}
	return cardChoice;
}

int main()
{

	srand(time(NULL));

	vector<string> player;
	vector<string> ai;
	int handSize;
	int gameRound = 1;
	int playerCash = 0;
	int distanceLeft = 30;
	int distanceBet;
	int multiplier;
	bool draw = false;

	cout << "Welcome, to your success, or your demise...." << endl;
	cout << "Presenting ----- Emperor Card!" << endl;
	cout << "Rules are simple, you bet, and you win or lose." << endl;
	cout << "Emperor beats Civilian, Civilian beats Slave, Slave beats Emperor. That's all you need to know. Heh." << endl;
	cout << "Let's start!" << endl;
	system("pause");
	system("cls");

	while (gameRound <= 12 && distanceLeft > 0)
	{
		cout << "Money left: " << playerCash << endl;
		cout << "Millimeters Left: " << distanceLeft << endl;
		system("pause");
		system("cls");

		cout << "Round " << gameRound << " of 12" << endl;
		printPlayer(gameRound);
		cout << endl;
		getBet(distanceLeft, distanceBet);

		giveCards(player, ai, gameRound);
		handSize = player.size();
		multiplier = winMultiplier(gameRound, distanceBet);
		system("cls");

		do
		{
				cout << "Pick your card:" << endl;
				handSize = player.size();
				printList(player, handSize);
				int playerCard = playerChoice(handSize) - 1; // Index starts at 0(Zero), hence - 1
				int aiCard = rand() % ai.size();
				system("cls");
				evalAndPay(player, ai, playerCard, aiCard, playerCash, multiplier, distanceLeft, distanceBet, draw);
				system("cls");
		} 
			while (draw == true && handSize > 1);

		player.clear();
		ai.clear();
		gameRound++;
	}

	if (distanceLeft > 0 && playerCash >= 20000000) // "Best" ending
	{
		cout << "...." << endl << endl << "You've done it...." << endl;
		cout << "You've beaten me... At my own game..." << endl;
		cout << "Congratulations then..." << endl;
		system("pause");
		system("cls");
	}
	else if (distanceLeft > 0 && playerCash < 20000000) // "Meh" ending
	{
		cout << "Ah, you've barely made it to the end, such a shame you didn't win 20,000,000 yen." << endl;
		cout << "Go, leave before I come for you..." << endl;
		system("pause");
		system("cls");
	}
	else if (distanceLeft <= 0 && playerCash < 20000000) // "Worst" ending
	{
		cout << "HA!...... HAHAHAHA!.......... HAHAHAHAHAHAHAHAHAHAHAHA!" << endl;
		system("pause");
		system("cls");
		cout << "*you hear nothing but the laughter of an evil being, as the drill continues to push towards your brain*" << endl;
		system("pause");
		system("cls");
		cout << "*muffled* I guess, I'm the Emperor here, huh?" << endl;
		cout << "*You slowly feel yourself fading away...*" << endl;
		cout << "You have died..." << endl;
		system("pause");
		system("cls");
	}
	system("pause");
	system("cls");
}