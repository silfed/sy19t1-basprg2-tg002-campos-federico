#include "pch.h"
#include <iostream>
#include <time.h>
#include <string>

using namespace std;

#ifndef NODE_H
#define NODE_H

struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};

#endif //NODE_H

Node* createNode(int size) //Creates node with user-input based size
{
	Node* first = new Node;
	Node* last = first;

	for (int x = 1; x < size; x++)
	{
		Node* madeNode = new Node;
		last->next = madeNode;
		last = last->next;
	}
	last->next = first;

	return first;
}

Node* createKnight(Node* knights, string input) //Creates a knight with a name
{
	knights->name = input;
	knights = knights->next;

	return knights;
}

Node* startPoint(Node* knights, int spoint) //Picks the starting point
{
	for (int x = 0; x < spoint; x++)
	{
		knights = knights->next;
	}
	return knights;
}

Node* eliminateKnight(Node* knights, int kill) //Eliminates a knight and connects the nodes
{
	Node* last = new Node;

	for (int x = 0; x < kill; x++)
	{
		last = knights;
		knights = knights->next;
	}

	last->next = knights->next;
	delete knights;
	last = last->next;
	return last;
}

void printKnights(Node* knights, int count) //Prints the names
{
	for (int x = 0; x < count; x++)
	{
		cout << knights->name << endl;
		knights = knights->next;
	}
	system("pause");
}

void acquireElimPrint(Node* knights, int kill) //Gets and prints the eliminated knights
{
	Node* temp = knights;
	for (int x = 0; x < kill; x++)
	{
		temp = temp->next;
	}
	cout << temp->name << " has been eliminated. Continue on.." << endl;
}

int main()
{
	srand(time(NULL));

	int n;
	int round = 1;
	string input;
	Node *knights = new Node;

	cout << "Welcome to the Watch of the Night!" << endl;
	cout << "How many knights have you brought with you? ";
	cin >> n;
	knights = createNode(n);

	cout << "Kindly introduce your " << n << " knights: " << endl;
	for (int x = 1; x <= n; x++)
	{
		cin >> input;
		knights = createKnight(knights, input);
	}
	int randStart = rand() % n + 1;

	for(int x = n; x > 1; x--)
	{
		knights = startPoint(knights, randStart);
		cout << "x=============================================x\n";
		cout << "                  ROUND " << round << endl;
		cout << "x=============================================x\n";
		cout << "Remaining Soldiers:" << endl;
		printKnights(knights, x);
		cout << endl;

		int posElim = rand() % x + 1; //Position to be eliminated
		cout << "Result:" << endl;
		cout << knights->name << " has chosen the number " << posElim << endl;
		acquireElimPrint(knights, posElim);
		knights = eliminateKnight(knights, posElim); //Removes eliminated knight
		round++;

		system("pause");
		system("cls");
	}

	cout << "==================x==================\n";
	cout << "             FINAL RESULT" << endl;
	cout << "==================x==================\n";
	system("pause");
	cout << endl;
	cout << knights->name << " shall go and seek for reinforcements to aid in the battle!" << endl;
	system("pause");
	system("cls");
}