#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

void packageSort(int packList[7])
{
	int temp;
	for (int x = 0; x < 7; x++)
	{
		for (int y = x + 1; y < 7; y++)
		{
			if (packList[x] > packList[y])
			{
				temp = packList[x];
				packList[x] = packList[y];
				packList[y] = temp;
			}
		}
		cout << packList[x] << endl;
	}
}

int purchase(int &gold, int subGold)
{
	int goldVal;
	goldVal = gold - subGold;
	gold = goldVal;
	return goldVal;
}

int recommendedPack(int packList[7], int &gold, int subGold)
{
	string input;
	int goldVal;

	for (int x = 0; x < 7; x++)
	{
		if (subGold <= (packList[x] + gold))
		{
			cout << "Recommended Package for you: " << packList[x] << endl;
			cout << "Would you like to purchase?(Y/N): " << endl;
			cin >> input;
			if (input == "Y" || input == "y")
			{
				cout << "Transaction complete!\n" << "Thank you for your purchase!" << endl;
				goldVal = gold + packList[x];
				gold = goldVal;
				return gold;
			}
			else if (input == "N" || input == "n")
			{
				cout << "Transaction cancelled..." << endl;
				system("pause");
				break;
			}
		}
	}
}

int main()
{
	string input;
	int gold = 250;
	int subGold;
	int packList[7] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };

	cout << "Welcome adventurer! Come to buy some items eh?" << endl;
	system("pause");

	while (true)
	{
		cout << "You currently have: " << gold << " gold" << endl;
		cout << "Care to tell me how much your desired item costs?" << endl;
		cin >> subGold;

		if (gold < subGold)
		{
			cout << "Sorry pal, you've got insufficient gold there, recommend ya buy a pack! Here's a list: " << endl;
			packageSort(packList);
			recommendedPack(packList, gold, subGold);
			purchase(gold, subGold);
			cout << "You have " << gold << " gold left." << endl;
			system("pause");
			system("cls");
		}
		else
		{
			cout << "Alright! You've got just the right amount of gold there! Here ya go!" << endl;
			purchase(gold, subGold);
			cout << "You have " << gold << " gold left." << endl;

			system("pause");
			system("cls");
		}

		cout << "Would you like to continue purchasing?(Y/N): ";
		cin >> input;
		if (input == "N" || input == "n")
		{
			break;
		}
	}
	system("pause");
}
