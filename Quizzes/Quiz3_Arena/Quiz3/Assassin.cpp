#include "pch.h"
#include "Assassin.h"


Assassin::Assassin(string name)
{
	mName = name;
	mHp = 30;
	mPow = 8;
	mVit = 4;
	mAgi = 9;
	mDex = 9;
}


Assassin::~Assassin()
{
}

string Assassin::getName()
{
	return mName;
}

int Assassin::getHp()
{
	return mHp;
}

int Assassin::getPow()
{
	return mPow;
}

int Assassin::getVit()
{
	return mVit;
}

int Assassin::getAgi()
{
	return mAgi;
}

int Assassin::getDex()
{
	return mDex;
}

void Assassin::takeDmg(int dmg)
{
	if (dmg < 1) return;
	mHp -= dmg;
	if (mHp < 0) mHp = 0;
}

void Assassin::attack(Assassin * enemy)
{
	int hitPercent = (getDex() / enemy->getAgi()) * 100;
	if (hitPercent >= 20 && hitPercent <= 80)
	{
		int dmg = (getPow() - enemy->getVit());
		if (dmg < 0) dmg = 1;
		enemy->takeDmg(dmg);

	}
	else if (hitPercent < 20)
	{
		hitPercent = 20;
		cout << "Your attack has missed." << endl;
		return;
	}
}


int Assassin::healHp(Assassin * self)
{
	mHp += (0.3 * 30);
	return mHp;
}

int Assassin::statAdd(Assassin * self)
{
	mAgi += 3;
	mDex += 3;
	return mAgi;
	return mDex;
}
