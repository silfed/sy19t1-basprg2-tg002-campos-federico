#pragma once
#include <string>
#include <iostream>

using namespace std;

class Warrior
{
public:
	Warrior(string name);
	~Warrior();

	string getName();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	void takeDmg(int dmg);
	void attack(Warrior* enemy);
	int healHp(Warrior* self);
	int statAdd(Warrior* self);

private:
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};

