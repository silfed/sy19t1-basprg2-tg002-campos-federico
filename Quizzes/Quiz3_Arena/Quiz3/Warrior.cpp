#include "pch.h"
#include "Warrior.h"


Warrior::Warrior(string name)
{
	mName = name;
	mHp = 50;
	mPow = 7;
	mVit = 9;
	mAgi = 8;
	mDex = 6;
}

Warrior::~Warrior()
{
}

string Warrior::getName()
{
	return mName;
}

int Warrior::getHp()
{
	return mHp;
}

int Warrior::getPow()
{
	return mPow;
}

int Warrior::getVit()
{
	return mVit;
}

int Warrior::getAgi()
{
	return mAgi;
}

int Warrior::getDex()
{
	return mDex;
}

void Warrior::takeDmg(int dmg)
{
	if (dmg < 1) return;
	mHp -= dmg;
	if (mHp < 0) mHp = 0;
}

void Warrior::attack(Warrior * enemy)
{
	int hitPercent = (getDex() / enemy->getAgi()) * 100;
	if (hitPercent >= 20 && hitPercent <= 80)
	{
		int dmg = (getPow() - enemy->getVit());
		if (dmg < 0) dmg = 1;
		enemy->takeDmg(dmg);

	}
	else if (hitPercent < 20)
	{
		hitPercent = 20;
		cout << "Your attack has missed." << endl;
		return;
	}
}

int Warrior::healHp(Warrior * self)
{
	mHp += (0.3 * 50);
	return mHp;
}

int Warrior::statAdd(Warrior * self)
{
	mHp += 3;
	mVit += 3;
	return mHp;
	return mVit;
}
