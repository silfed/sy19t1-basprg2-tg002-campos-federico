#pragma once
#include <string>
#include <iostream>

using namespace std;

class Assassin
{
public:
	Assassin(string name);
	~Assassin();

	string getName();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	void takeDmg(int dmg);
	void attack(Assassin* enemy);
	int healHp(Assassin* self);
	int statAdd(Assassin* self);

private:
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};

