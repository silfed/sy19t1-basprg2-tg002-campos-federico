#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include "Wiz.h"
#include "Warrior.h"
#include "Assassin.h"
#include <time.h>

using namespace std;

void fillVector(vector<string>& charJob)
{
	charJob.push_back("== Warrior ==");
	charJob.push_back("== Assassin ==");
	charJob.push_back("== Mage ==");
}

void printVector(vector<string>& charJob)
{
	for (int x = 0; x < charJob.size(); x++)
	{
		cout << x + 1 << ". " << charJob[x] << endl;
	}
}


int main()
{
	vector<string> plCharJob;
	vector<string> aiCharJob;

	fillVector(plCharJob);
	printVector(plCharJob);
	system("pause");
}
