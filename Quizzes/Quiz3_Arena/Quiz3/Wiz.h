#pragma once
#include <string>
#include <iostream>

using namespace std;

class Wiz
{
public:
	Wiz(string name);
	~Wiz();

	string getName();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	void takeDmg(int dmg);
	void attack(Wiz* enemy);
	int healHp(Wiz* self);
	int statAdd(Wiz* self);

private:
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};

