#include "pch.h"
#include "Wiz.h"


Wiz::Wiz(string name)
{
	mName = name;
	mHp = 40;
	mPow = 12;
	mVit = 5;
	mAgi = 7;
	mDex = 9;
}

Wiz::~Wiz()
{
}

string Wiz::getName()
{
	return mName;
}

int Wiz::getHp()
{
	return mHp;
}


int Wiz::getPow()
{
	return mPow;
}

int Wiz::getVit()
{
	return mVit;
}

int Wiz::getAgi()
{
	return mAgi;
}

int Wiz::getDex()
{
	return mDex;
}

void Wiz::takeDmg(int dmg)
{
	if (dmg < 1) return;
	mHp -= dmg;
	if (mHp < 0) mHp = 0;
}

void Wiz::attack(Wiz * enemy)
{
	int hitPercent = (getDex() / enemy->getAgi()) * 100;
	if (hitPercent >= 20 && hitPercent <= 80)
	{
		int dmg = (getPow() - enemy->getVit());
		if (dmg < 0) dmg = 1;
		enemy->takeDmg(dmg);
		
	}
	else if (hitPercent < 20)
	{
		hitPercent = 20;
		cout << "Your attack has missed." << endl;
		return;
	}

}

int Wiz::healHp(Wiz* self)
{
	mHp += (0.3 * 40);
	return mHp;
}

int Wiz::statAdd(Wiz* self)
{
	mPow += 5;
	return mPow;
}
