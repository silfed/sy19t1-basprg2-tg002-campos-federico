#pragma once
#include <string>

using namespace std;

class Unit
{
public:
	Unit();
	~Unit();

private:
	string mName;
	int mHP;
	int mPOW;
	int mVIT;
	int mAGI;
	int mDEX;
};

