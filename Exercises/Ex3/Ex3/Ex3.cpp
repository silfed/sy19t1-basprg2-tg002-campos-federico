#include "pch.h"
#include <iostream>
#include <time.h>

using namespace std;

void randomFill(int* arr1) //fills array with random values from 0 - 99
{
	for (int x = 0; x < 15; x++)
	{
		arr1[x] = rand() % 100;
		cout << arr1[x] << " "; //proof that array was filled
	}
	system("pause");
}

int* randomArray(int size, int range) //random array generator, user based sized and range for values of the array
{
	int* arr2 = new int[size];

	for (int x = 0; x < size; x++)
	{
		arr2[x] = rand() % range;
		cout << arr2[x] << " ";
	}

	return arr2;
}


int main()
{
	srand(time(NULL));
	int* array1 = new int[15]; //using new to be able to use delete[]
	randomFill(array1);
	delete[] array1;

	int* array2 = randomArray(15, 200);
	delete[] array2;

	system("pause");
}