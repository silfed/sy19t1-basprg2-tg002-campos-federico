#pragma once
#include "Shape.h"

using namespace std;

class Square : public Shape

{
public:
	Square();
	Square(string name, int sides);
	~Square();

	float getLength();
	void setLength(float length);
	float getArea() override;

private:
	float mLength;
};

