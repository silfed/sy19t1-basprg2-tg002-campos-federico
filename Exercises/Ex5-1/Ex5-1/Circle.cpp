#include "pch.h"
#include "Circle.h"


Circle::Circle()
	: Shape("Circle", 0)
{
}

Circle::~Circle()
{
}

float Circle::getRadius()
{
	return mRadius;
}

void Circle::setRadius(float radius)
{
	mRadius = radius;
}

float Circle::getArea()
{
	return 3.14 * mRadius * mRadius;
}
