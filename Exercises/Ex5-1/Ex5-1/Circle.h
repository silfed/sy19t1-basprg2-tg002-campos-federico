#pragma once
#include "Shape.h"

using namespace std;

class Circle : public Shape

{
public:
	Circle();
	~Circle();

	float getRadius();
	void setRadius(float radius);
	float getArea() override;

private:
	float mRadius;
};

