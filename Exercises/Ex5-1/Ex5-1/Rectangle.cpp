#include "pch.h"
#include "Rectangle.h"


Rectangle::Rectangle()
	:Shape("Rectangle", 4)
{
}

Rectangle::Rectangle(string name, int sides)
	: Shape(name, sides)
{
}


Rectangle::~Rectangle()
{
}

float Rectangle::getLength()
{
	return mLength;
}

void Rectangle::setLength(float length)
{
	mLength = length;
}

float Rectangle::getWidth()
{
	return mWidth;
}

void Rectangle::setWidth(float width)
{
	mWidth = width;
}

float Rectangle::getArea()
{
	return mLength * mWidth;
}
