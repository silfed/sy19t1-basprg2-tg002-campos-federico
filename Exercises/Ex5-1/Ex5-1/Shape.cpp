#include "pch.h"
#include "Shape.h"


Shape::Shape(string name, int sides)
{
	mName = name;
	mNumSides = sides;
}

Shape::~Shape()
{
}

string Shape::getName()
{
	return mName;
}

void Shape::setSides(int sides)
{
	mNumSides = sides;
}

int Shape::getSides()
{
	return mNumSides;
}

float Shape::getArea()
{
	return 0.0f;
}
