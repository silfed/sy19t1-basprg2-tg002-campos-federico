#pragma once
#include "Shape.h"

using namespace std;

class Rectangle : public Shape
{
public:
	Rectangle();
	Rectangle(string name, int sides);
	~Rectangle();

	float getLength();
	void setLength(float length);
	float getWidth();
	void setWidth(float width);
	float getArea() override;

private:
	float mLength;
	float mWidth;
};

