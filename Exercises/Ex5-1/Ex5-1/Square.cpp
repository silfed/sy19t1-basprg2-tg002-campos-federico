#include "pch.h"
#include "Square.h"

Square::Square()
	:Shape("Square", 4)
{
}

Square::Square(string name, int sides)
	: Shape(name, sides)
{
}


Square::~Square()
{
}

float Square::getLength()
{
	return mLength;
}

void Square::setLength(float length)
{
	mLength = length;
}

float Square::getArea()
{
	return mLength * mLength;
}