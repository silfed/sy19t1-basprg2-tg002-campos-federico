#include <iostream>
#include <conio.h>
#include "pch.h"
#include <vector>
#include <string>
#include "Square.h"
#include "Circle.h"
#include "Rectangle.h"

using namespace std;

void printShapes(const vector<Shape*>& shapes)
{
	for (int x = 0; x < shapes.size(); x++)
	{
		Shape* shape = shapes[x];
		cout << shape->getName() << " Area: " << shape->getArea() << endl;
		cout << "# of Sides: " << shape->getSides() << endl << endl;
	}

}

int main()
{
	vector<Shape*> shapes;

	Square* square = new Square();
	square->setLength(5.0f);
	square->setSides(4);
	shapes.push_back(square);

	Circle* circle = new Circle();
	circle->setRadius(10.0f);
	circle->setSides(0);
	shapes.push_back(circle);

	Rectangle* rectangle = new Rectangle();
	rectangle->setLength(10.0f);
	rectangle->setWidth(8.0f);
	rectangle->setSides(4);
	shapes.push_back(rectangle);

	printShapes(shapes);
	system("pause");
}

