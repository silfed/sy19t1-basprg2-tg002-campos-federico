#pragma once
#include "Unit.h"

class Might : public Unit
{
public:
	Might(string name);
	~Might();

	int castSkill() override;

private:
	int mPower = 2;
};

