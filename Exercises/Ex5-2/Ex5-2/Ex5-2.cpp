#include "pch.h"
#include <iostream>
#include <vector>
#include <time.h>
#include "Unit.h"
#include "Heal.h"
#include "Concentration.h"
#include "Haste.h"
#include "IronSkin.h"
#include "Might.h"

using namespace std;

void printUnit()
{
	Unit* warrior = new Unit("Therion");
	cout << "Name: " << warrior->getName() << endl;
	cout << "HP: " << warrior->getHp() << endl;
	cout << "Power: " << warrior->getPwr() << endl;
	cout << "Vitality: " << warrior->getVit() << endl;
	cout << "Dexterity: " << warrior->getDex() << endl;
	cout << "Agility: " << warrior->getAgi() << endl;
}

void printSkills(vector<Unit*>& skills)
{
	cout << "Here are your skills to be casted: " << endl;
	for (int x = 0; x < skills.size(); x++)
	{
		cout << x + 1 << ".)" << skills[x] << endl;
	}
}

int main()
{
	srand(time(NULL));
	vector<Unit*> skillz;
	Heal* heal = new Heal("Heal");
	skillz.push_back(heal);

	Haste* haste = new Haste("Haste");
	skillz.push_back(haste);

	Concentration* concentration = new Concentration("Concentration");
	skillz.push_back(concentration);

	IronSkin* ironSkin = new IronSkin("Iron Skin");
	skillz.push_back(ironSkin);

	Might* might = new Might("Might");
	skillz.push_back(might);
	
	printSkills(skillz);
	printUnit();
	system("pause");
}
