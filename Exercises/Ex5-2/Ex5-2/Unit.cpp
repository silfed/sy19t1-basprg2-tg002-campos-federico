#include "pch.h"
#include "Unit.h"


Unit::Unit()
{
}

Unit::Unit(string name)
{
	mName = name;
}


Unit::~Unit()
{
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getPwr()
{
	return mPwr;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::castSkill()
{
	return 0;
}

