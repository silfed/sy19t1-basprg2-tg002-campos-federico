#pragma once
#include "Unit.h"

class IronSkin : public Unit
{
public:
	IronSkin(string name);
	~IronSkin();

	int castSkill() override;

private:
	int mVitality = 2;
};

