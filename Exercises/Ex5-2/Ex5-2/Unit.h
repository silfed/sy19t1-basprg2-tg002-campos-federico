#pragma once
#include <string>
#include <iostream>

using namespace std;

class Unit
{
public:
	Unit();
	Unit(string name);
	~Unit();

	string getName();
	int getHp();
	int getPwr();
	int getDex();
	int getVit();
	int getAgi();

	virtual int castSkill();

protected:
	int mHp = 100;
	string mName;
	int mPwr = 1;
	int mDex = 1;
	int mVit = 1;
	int mAgi = 1;
};

