#pragma once
#include "Unit.h"

class Haste : public Unit

{
public:
	Haste(string name);
	~Haste();

	int castSkill() override;

private:
	int mAgility = 2;
};

