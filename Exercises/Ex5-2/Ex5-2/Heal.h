#pragma once
#include "Unit.h"

class Heal : public Unit
{
public:
	Heal(string name);
	~Heal();

	int castSkill() override;

private:
	int mHeal = 10;
};

