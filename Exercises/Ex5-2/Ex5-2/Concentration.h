#pragma once
#include "Unit.h"

class Concentration : public Unit
{
public:
	Concentration(string name);
	~Concentration();

	int castSkill() override;

private:
	int mDexterity = 2;
};

