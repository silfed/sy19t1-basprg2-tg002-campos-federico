#pragma once
#include "pch.h"
#include <conio.h>
#include <iostream>
#include <string>

using namespace std;

class CloudInfo
{
public:
	CloudInfo();
	~CloudInfo();

	int getAttack();
	int getAttackPercentage();
	int getDefense();
	int getDefensePercentage();
	int getMagicAtk();
	int getMagicDef();
	int getMagicDefPercent();

private:
	string name;
	int mHp;
	int mMp;
	int mXp;
	int mStr;
	int mDex;
	int mVit;
	int mMag;
	int mSprt;
	int mLuk;
	int mAtk;
	int mDef;
};

