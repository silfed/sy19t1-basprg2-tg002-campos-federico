#pragma once
#include "pch.h"
#include <conio.h>
#include <iostream>
#include <string>

using namespace std;

class CloudInfo
{
public:
	CloudInfo();
	CloudInfo(
		string name,
		int hp,
		int mp,
		int xp,
		int str,
		int dex,
		int vit,
		int mag,
		int sprt,
		int luk
		);

	void getAttack();
	void getAttackPercentage();
	void getDefense();
	void getDefensePercentage();
	void getMagicAtk();
	void getMagicDef();
	void getMagicDefPercent();

	void basicAttack();
	void useItem();
	void guard();
	void useMagic();

	void fourCut();
	void magic();
	void dBlow();
	void wItem();
};

