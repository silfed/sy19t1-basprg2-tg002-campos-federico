#include "pch.h"
#include <iostream>
#include <time.h>
#include <string>

using namespace std;

void bet1(int gold) // gets user's bet
{
	cout << "Your current gold: " << gold << endl;
	cout << "Input bet: ";
	cin >> gold;
}

int bet2(int& gold) // gets user's bet, deducts from player's gold, returns bet value
{
	int playerGold1 = gold;
	int tempBet; // value holder for bet

	cout << "Your current gold: " << gold << endl;
	cout << "Input bet: ";
	cin >> tempBet;
	playerGold1 -= tempBet;

	return tempBet;
}

void bet3(int& gold, int& bet)  // gets user's bet, deducts from player's gold, bet value set
{
	while (true)
	{
		cout << "Your current gold: " << gold << endl;
		cout << "Input bet: ";
		cin >> bet;

		if (bet == 0 || bet > gold)
		{
			cout << "You have placed an invalid bet, please input again.." << endl;
			system("pause");
			system("cls");
		}
		else if (bet > 0 || bet <= gold)
		{
			break;
		}
	}
	gold -= bet;
}

int diceRoll(int& d1, int& d2) // random dice roll from 1 - 6
{
	d1 = (rand() % 6) + 1;
	d2 = (rand() % 6) + 1;

	int diceSum;
	diceSum = d1 + d2;
	return diceSum;
}

void payout(int& gold, int bet, int userSum, int aiSum) // payout for different conditions
{
	if (userSum == 2)
	{
		bet *= 3;
		cout << "Snake eyes! You've won " << bet << "!" << endl;
		gold += bet;
	}
	else if (aiSum == 2)
	{
		cout << "Dang! Snake eyes for the AI! You just lost " << bet << "!" << endl;
	}
	else if (userSum > aiSum)
	{
		bet *= 2;
		cout << "Nice roll! You've won " << bet << "!" << endl;
		gold += bet;
	}
	else if (userSum < aiSum)
	{
		cout << "That's sad, you just lost " << bet << "!" << endl;
	}
	else if (userSum == aiSum)
	{
		cout << "Woops! That's a draw! You get your bet back." << endl;
		gold += bet;
	}
}

void playRound(int& gold, int bet, int userSum, int aiSum)
{
	cout << "Hello! Welcome to Dice Mania!" << endl;
	cout << "Now that you've entered your bet, let the dice roll!" << endl;
	system("pause");
	system("cls");

	cout << "It's your turn! Roll the dice!" << endl;
	system("pause");
	cout << "You have rolled " << userSum << endl;
	system("pause");
	system("cls");

	cout << "AI turn next.." << endl;
	system("pause");
	system("cls");

	cout << "AI is rolling the dice!" << endl;
	cout << "AI has rolled " << aiSum << endl;
	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));

	int gold = 1000;
	bool run = true;
	while (run)
	{
		int d1, d2, bet;
		int userSum = diceRoll(d1, d2);
		int aiSum = diceRoll(d1, d2);
		string x;

		bet3(gold, bet);
		playRound(gold, bet, userSum, aiSum);
		payout(gold, bet, userSum, aiSum);

		if (gold <= 0)
		{
			cout << "You have lost all your gold, better luck next time!" << endl;
			system("pause");
			system("cls");
			run = false;
		}

		cout << "Would you like to play again? (Y/N): ";
		cin >> x;

		if (x == "N" || x == "n")
		{
			cout << "Thank you for playing! Good bye!" << endl;
			run = false;
		}
	}
}
