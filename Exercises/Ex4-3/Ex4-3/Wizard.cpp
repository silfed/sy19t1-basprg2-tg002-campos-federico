#include "pch.h"
#include "Wizard.h"
#include "Spell.h"


Wizard::Wizard()
{
}
Wizard::Wizard(string name, int hp, int mp)
{
	mName = name;
	mHp = hp;
	mMp = mp;
}
Wizard::~Wizard()
{
}
string Wizard::getName()
{
	return mName;
}
int Wizard::getHp()
{
	return mHp;
}
int Wizard::getMp()
{
	return mMp;
}
Spell * Wizard::getSpell()
{
	return mSpell;
}
Spell *Wizard::setSpell(Spell *spell)
{
	if (spell != nullptr)
	{
		delete mSpell;
	}
	mSpell = spell;
	return mSpell;
}
void Wizard::hpDeduct(int dmg)
{
	mHp -= dmg;
}
void Wizard::mpDeduct(int mp)
{
	mMp -= mp;
}
void Wizard::castSpell(Wizard* caster, Wizard* enemy)
{
	int dmg = mSpell->getDmg();
	int cost = mSpell->getMana();
	enemy->hpDeduct(dmg);
	caster->mpDeduct(cost);
	cout << caster->getName() << " has just casted " << caster->getSpell()->getName() << "! Dealt " << mSpell->getDmg() << " damage!" << endl;
	cout << "Remaining HP for " << enemy->getName() << ": " << enemy->getHp() << endl;
	cout << "Remaining MP for " << caster->getName() << ": " << caster->getMp() << endl;
}
