#pragma once
#include <iostream>
#include <string>

using namespace std;

class Spell;
class Wizard
{
public:
	Wizard();
	Wizard(string name, int hp, int mp);
	~Wizard();

	string getName();
	int getHp();
	int getMp();
	Spell* getSpell();
	Spell* setSpell(Spell* spell);
	void hpDeduct(int dmg);
	void mpDeduct(int mp);
	void castSpell(Wizard* caster, Wizard* enemy);

private:
	int mHp;
	int mMp;
	string mName;
	Spell* mSpell;
};

