#include "pch.h"
#include "Spell.h"

Spell::Spell()
{
	mName = "Stone";
	mDmg = 5;
	mMana = 5;
}

Spell::Spell(string name, int damage, int mana)
{
	mName = name;
	mDmg = damage;
	mMana = mana;
}

Spell::~Spell()
{
}

string Spell::getName()
{
	return mName;
}

int Spell::getDmg()
{
	return mDmg;
}

int Spell::getMana()
{
	return mMana;
}
