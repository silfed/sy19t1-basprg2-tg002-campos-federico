#include "pch.h"
#include <iostream>
#include <string>
#include "Spell.h"
#include "Wizard.h"
using namespace std;

int main()
{
	Wizard *Rubick = new Wizard("Rubick", 5000, 3000);
	Spell *spell1 = new Spell("Inferno", 300, 100);
	Wizard *Zeus = new Wizard("Zeus", 6000, 2000);
	Spell *spell2 = new Spell("Hellfire", 250, 82);

	Rubick->setSpell(spell1);
	Zeus->setSpell(spell2);

	cout << "==== Welcome Adventurer, meet your wizards! ====" << endl;
	cout << "Wizard # 1 -" << endl;
	cout << "Name: " << Rubick->getName() << endl;
	cout << "Health: " << Rubick->getHp() << endl;
	cout << "Mana: " << Rubick->getMp() << endl << endl;
	cout << "Wizard # 2 -" << endl;
	cout << "Name: " << Zeus->getName() << endl;
	cout << "Health: " << Zeus->getHp() << endl;
	cout << "Mana: " << Zeus->getMp() << endl << endl;
	cout << "==============================================" << endl;

	while (Rubick->getHp() && Zeus->getHp() > 0)
	{
		Rubick->castSpell(Rubick, Zeus);
		system("pause");
		cout << "==================================" << endl;
		Zeus->castSpell(Zeus, Rubick);
		system("pause");
		system("CLS");

		if (Rubick->getHp() <= 0 || Rubick->getMp() <= 0)
		{
			cout << Zeus->getName() << " has won!" << endl;
			break;
		}
		else if (Zeus->getHp() <= 0 || Zeus->getMp() <= 0)
		{
			cout << Rubick->getName() << " has won!" << endl;
			break;
		}
	}
	system("pause");
}
