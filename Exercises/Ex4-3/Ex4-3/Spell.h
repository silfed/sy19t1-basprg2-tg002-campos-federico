#pragma once
#include "string"

using namespace std;

class Spell
{
public:
	Spell();
	Spell(string name, int damage, int mana);
	~Spell();

	string getName();
	int getDmg();
	int getMana();

private:
	string mName;
	int mDmg;
	int mMana;
};

