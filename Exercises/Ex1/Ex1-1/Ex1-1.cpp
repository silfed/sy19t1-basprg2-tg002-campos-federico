#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int factorial(int n)
{
	int prod = 1;
	for (int x = 1; x <= n; x++)
	{
		prod = prod * x;
	}
	return prod;
}

void printArr(string items[8])
{
	for (int x = 0; x <= 7; x++)
	{
		cout << items[x] << endl;
	}
}

void countArr(string items[8])
{
	for (int x = 0; x <= 8; x++)
	{
		if (x == 8)
		{
			cout << "There are currently " << x << " items in your inventory." << endl;
		}
	}
}

void randomFill(int arr1[10])
{
	srand(time(NULL));
	for (int x = 0; x < 10; x++)
	{
		arr1[x] = rand() % 100;
		cout << arr1[x] << ", ";
	}
}

int largeVal(int arr2[10])
{
	int biggest = arr2[0];
	for (int x = 0; x < 10; x++)
	{
		if (arr2[x] > biggest)
		{
			biggest = arr2[x];
		}
	}
	return biggest;
}

void sortArr(int arr3[10])
{
	int temp;
	for (int x = 0; x < 10; x++)
	{
		for (int y = x + 1; y < 10; y++)
		{
			if (arr3[x] > arr3[y])
			{
				temp = arr3[x];
				arr3[x] = arr3[y];
				arr3[y] = temp;
			}
		}
		cout << arr3[x] << ", ";
	}
}

int main()
{
	int input;
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	int randArray[10] = {};

	cout << "Please input a number for factorial: ";
	cin >> input;
	cout << "Factorial of " << input << ": " << factorial(input) << endl;
	system("pause");

	cout << endl << "Inventory:" << endl;
	printArr(items);

	cout << endl;
	countArr(items);
	system("pause");

	cout << endl;
	randomFill(randArray);
	system("pause");

	cout << "Largest value of your array: " << largeVal(randArray) << endl;
	cout << "Sorting array..." << endl;
	system("pause");
	sortArr(randArray);
	system("pause");
}

