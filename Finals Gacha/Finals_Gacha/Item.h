#pragma once
#include <string>
#include <iostream>
#include "Character.h"

using namespace std;

class Item
{
public:
	Item(string name);

	string getName();
	int getRarity();

	virtual void trigger(Character *player);

protected:
	string mName;
	int mRarity;
};