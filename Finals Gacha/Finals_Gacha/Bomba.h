#pragma once
#include "Item.h"

class Bomba : public Item
{
public:
	Bomba(string name);

	void trigger(Character* player);
};