#pragma once
#include "Item.h"

class R : public Item
{
public:
	R(string name);

	void trigger(Character* player);
};

