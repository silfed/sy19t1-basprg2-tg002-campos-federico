#include "pch.h"
#include "Bomba.h"


Bomba::Bomba(string name) : Item(name)
{
}

void Bomba::trigger(Character* player)
{

	int damage = 25;
	player->deductCrystal(5);
	cout << "You pulled a " << this->getName() << "! Oof, bad luck!" << endl;
	cout << "You were dealt " << damage << " damage! Better pull a health potion fast :o" << endl;
	player->deductHP(damage);

}



