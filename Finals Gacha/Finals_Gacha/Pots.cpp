#include "pch.h"
#include "Pots.h"


Pots::Pots(string name) : Item(name)
{
}

void Pots::trigger(Character* player)
{

	player->deductCrystal(5);
	cout << "You've pulled a " << this->getName() << "! This'll help ya get through the bombs!" << endl;
	int HPgain = 30;
	cout << "You've gained " << HPgain << " Health points! Hope ya don't pull a bomb next!" << endl;
	player->addHP(HPgain);

}


