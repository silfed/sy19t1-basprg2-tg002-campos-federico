#include "pch.h"
#include <iostream>
#include "Item.h"
#include "Character.h"
#include <time.h>
using namespace std;

int main()
{
	srand(time(NULL));
	cout << "Welcome, youngling, to Generic Gatcha!" << endl;
	cout << "It's time to test out your luck, and see if you're a Gacha Lord." << endl;
	cout << "May RNGesus be on your side~" << endl;
	system("pause");
	system("cls");

	Character* player = new Character(100, 100);

	while (player->canPlay())
	{
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		player->printStats();
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << endl;
		
		player->pullItem();
		cout << "------------------------" << endl;
		system("pause");
		system("cls");
	}

	if (player->hasWon())
	{
		cout << "You've collected " << player->getRarityPoints() << " rarity points! Congratulations!" << endl;
		cout << "Well then, I guess you truly are a Gacha Lord! See ya around~" << endl;
	}
	else if (player->isDead())
	{
		cout << "Your body has exploded into bits due to repeated pulls of a bomb. Yikes." << endl;
		cout << "== Death by Bomb ==" << endl;
	}
	else if (player->noCrystals())
	{
		cout << "You ran out of crystals. Guess you aren't a Gacha Lord after all." << endl;
		cout << "== Game Over ==" << endl;
	}
	

	cout << endl << "~~ Game Summary ~~" << endl;
	cout << "~~~~~~~~~~~~~~~~~~" << endl;
	cout << "Items collected: " << endl;
	player->collatePulls();
	cout << "~~~~~~~~~~~~~~~~~~" << endl;
	cout << "Thanks for playing! And please, don't get addicted to gachas D:" << endl;
	cout << "It's for your own sake! D:" << endl;
	system("pause");

}