#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Item;
class Character
{
public:
	Character(int HP, int crystal);

	int getHP();
	int getCrystal();
	int getRarityPoints();

	void deductCrystal(int value);
	void addCrystal(int value);
	void deductHP(int value);
	void addHP(int value);
	void addRarityPoints(int value);

	void printStats();
	void pullItem();
	void collatePulls();

	bool canPlay();
	bool isDead();
	bool noCrystals();
	bool hasWon();

private:
	int mHP;
	int mCrystal;
	int mRarityPoints;
	int mPulls;
	vector<Item*> inventory;
};