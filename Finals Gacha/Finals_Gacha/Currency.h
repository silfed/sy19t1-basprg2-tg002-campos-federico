#pragma once
#include "Item.h"

class Currency : public Item
{
public:
	Currency(string name);

	void trigger(Character* player);
};

