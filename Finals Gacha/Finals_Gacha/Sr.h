#pragma once
#include "Item.h"

class SR : public Item
{
public:
	SR(string name);

	void trigger(Character* player);
};

