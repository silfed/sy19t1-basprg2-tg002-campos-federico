#include "pch.h"
#include "Character.h"
#include "Item.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "Bomba.h"
#include "Pots.h"
#include "Currency.h"

Character::Character(int playerHP, int playerCrystal)
{
	mHP = playerHP;
	mCrystal = playerCrystal;
}

int Character::getHP()
{
	return mHP;
}

int Character::getCrystal()
{
	return mCrystal;
}

int Character::getRarityPoints()
{
	return mRarityPoints;
}

void Character::deductCrystal(int value)
{
	mCrystal -= value;
}

void Character::addCrystal(int value)
{
	mCrystal += value;
}

void Character::deductHP(int value)
{
	mHP -= value;
}

void Character::addHP(int value)
{
	mHP += value;
}

void Character::addRarityPoints(int value)
{
	mRarityPoints += value;
}

void Character::printStats()
{
	cout << "HP: " << mHP << endl;
	cout << "Crystals: " << mCrystal << endl;
	cout << "Rarity Points: " << mRarityPoints << endl;
	cout << "Pulls: " << mPulls << endl;
}

void Character::pullItem()
{
	int randNum;
	randNum = rand() % 100 + 1;
	int index = inventory.size();
	if (randNum <= 1)
	{
		inventory.push_back(new SSR("SSR"));
	}
	else if (randNum <= 10)
	{
		inventory.push_back(new SR("SR"));
	}
	else if (randNum <= 50)
	{
		inventory.push_back(new R("R"));
	}
	else if (randNum <= 65)
	{
		inventory.push_back(new Pots("Health Potion"));
	}
	else if (randNum <= 85)
	{
		inventory.push_back(new Bomba("Bomb"));
	}
	else if (randNum <= 100)
	{
		inventory.push_back(new Currency("Crystal"));
	}

	mPulls++;
	Item *itemGet = inventory[index];
	itemGet->trigger(this);
}

void Character::collatePulls()
{
	string items[6] = { "Health Potion", "Bomb", "Crystals", "R", "SR", "SSR" };
	int index = 0;

	for (int x = 0; x < 6; x++)
	{
		bool haveItem = false;
		int counter = 0;
		for (int y = 0; y < inventory.size(); y++)
		{
			Item *item = inventory[y];
			if (items[index] == item->getName())
			{
				haveItem = true;
				counter++;
			}
		}

		if (haveItem)
		{
			cout << items[index] << ": " << counter << "x" << endl;
		}
		index++;
	}
}

bool Character::canPlay()
{
	if (mHP > 0 && mCrystal > 0 && mRarityPoints < 100)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Character::isDead()
{
	if (mHP <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Character::noCrystals()
{
	if (mCrystal <= 0)
	{
		return true;
	}
	else
	{ 
		return false;
	}
		
}

bool Character::hasWon()
{
	if (mRarityPoints >= 100)
	{
		return true;
	}
	else
	{
		return false;
	}
}
