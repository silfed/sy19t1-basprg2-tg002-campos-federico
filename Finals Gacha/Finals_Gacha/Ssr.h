#pragma once
#include "Item.h"

class SSR : public Item
{
public:
	SSR(string name);

	void trigger(Character* player);
};

