#pragma once
#include "Item.h"

class Pots : public Item
{
public:
	Pots(string name);

	void trigger(Character* player);
}; 

