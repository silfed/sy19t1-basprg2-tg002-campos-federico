#include "pch.h"
#include "Currency.h"


Currency::Currency(string name) : Item(name)
{
}

void Currency::trigger(Character* player)
{

	player->deductCrystal(5);
	cout << "You pulled " << this->getName() << "!! 2 extra pulls right there!" << endl;
	int gainCrystal = 15;
	cout << "You've gained " << gainCrystal << " crystal!" << endl;
	player->addCrystal(gainCrystal);

}
